<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Minimal</title>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <link rel="stylesheet" href="style.css">
<script >
  document.addEventListener("DOMContentLoaded", function() { 
    document.querySelectorAll('textarea, input').forEach(function(e) {
        if(e.value === '') e.value = window.sessionStorage.getItem(e.name, e.value);
        e.addEventListener('input', function() {
            window.sessionStorage.setItem(e.name, e.value);
        })
    })

}); 
</script>
    
</head>
// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
<body>
  <div class="wrapper">
    <header id="header" class="header lock-padding">
      <div class="header__container _conteiner">
        <a href="" class="header__logo"></a>
        <a href="#popup" class="popup-link">Форма</a> 
      </div>
    </header>
  </div>
  <div class="popup" id="popup">
  <div class="popup__body">
    <div class="popup__content">
      <a href="##" class="popup__close"></a>
<form action="index.php" accept-charset="UTF-8"  method="POST">
<?php 
    if (!empty($messages)) {
      print('<div id="messages">');
      // Выводим все сообщения.
      foreach ($messages as $message) {
        print($message);
      }
      print('</div>');
    }
    ?>

Имя:</br>
<label <?php 
          if ($errors['name']) {print 'class="error-label"';} 
          ?> >

    <input type="text" name="name" placeholder="Введите имя" value="<?php print $errors['name'] ? $errors_value['name'] : $values['name'];?>">
</label><br/>
</br>
 <label <?php 
          if ($errors['email']) {print 'class="error-label"';} 
          ?>>
        Email:<br>
        <input id="email" type="email" name="email" value="<?php
         print $errors['email'] ? $errors_value['email'] : $values['email']; 
         ?>">
 </label><br>

    <label <?php 
          if ($errors['year']) {print 'class="error-label"';} 
          ?> >
        Год рождения:<br>
        <select name="year">
          <option>1980</option>
          <option>1981</option>
          <option>1982</option>
          <option>1983</option>
          <option>1984</option>
          <option>1985</option>
          <option>1986</option>
          <option>1987</option>
          <option>1988</option>
          <option>1989</option>
          <option>1990</option>
          <option>1991</option>
          <option>1992</option>
          <option>1993</option>
          <option>1994</option>
          <option>1995</option>
          <option>1996</option>
          <option>1997</option>
          <option>1998</option>
          <option>1999</option>
          <option>2000</option>
        </select>
        </label><br>

    Выберите пол:<br/>
    <label <?php 
          if ($errors['sex']) {print 'class="error-label"';}
          ?>>
        <input type="radio" name="sex" value="M" <?php if($values['sex'] == "M") {print "checked";} ?>> М
      </label>
      <label <?php 
          if ($errors['sex']) {print 'class="error-label"';}
          ?>>
        <input type="radio" name="sex" value="F" <?php if($values['sex'] == "F") {print "checked";} ?>> Ж
      </label> <br>
         
    Количество конечностей: <br>
        <label <?php 
          if ($errors['limbs']) {print 'class="error-label"';} 
          ?>>
          <input type="radio" name="limbs" value="4" <?php if($values['limbs'] == "5") {print "checked";} ?>> 5
        </label>
        <label <?php 
          if ($errors['limbs']) {print 'class="error-label"';} 
          ?>>
          <input type="radio" name="limbs" value="invalid" <?php if($values['limbs'] == "invalid") {print "checked";} ?>> инвалид
        </label>
        <label <?php 
          if ($errors['limbs']) {print 'class="error-label"';} 
          ?>>
          <input type="radio" name="limbs" value="octopuse" <?php if($values['limbs'] == "octopuse") {print "checked";} ?>> осьминог
        </label> <br>

        Ваши сверхспособности:
        <br/>
    <label>
         
         <select  name="superpower[]" multiple="multiple">
                <option value="Eat and don't get fat" selected="select">Кушать и не толстеть</option>
                <option value="Wake up without an alarm clock">Вставать без будильника</option>
                <option value="Understand mat.analysis">Понимать мат.анализ</option> 
          </select> 
    </label><br/>

    Биография: <br>
        <label>
          <textarea name="biography" rows="5" cols="50"></textarea>
        </label> <br>
    
      
        <label <?php 
          if ($errors['checkbox']) {print 'class="error-label"';} 
          ?>>
          <input type="checkbox" name="checkbox">
          С контрактом ознакомлен
        </label>
        
        <input type="submit" value="Отправить">

</form>
</div>
  </div>
</div>
<script src="popup.js"></script>

</body>


