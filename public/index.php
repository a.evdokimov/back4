<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();
  $errors_value = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }
  
  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['sp'] = !empty($_COOKIE['sp_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);

  // Выдаем сообщения об ошибках.
  if ($errors['name']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('name_error', '', 100000);
    // Выводим сообщение.
    $errors_value['name'] = empty($_COOKIE['name_error_value']) ? '' : $_COOKIE['name_error_value'];
    setcookie('name_error_value', '', 100000);
    $messages[] = '<div class="error">Неправильное имя</div>';
  }
  if ($errors['email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 100000);
    // Выводим сообщение.
    $errors_value['email'] = empty($_COOKIE['email_error_value']) ? '' : $_COOKIE['email_error_value'];
    setcookie('email_error_value', '', 100000);
    $messages[] = '<div class="error">Неправильный email</div>';
  }
  if ($errors['year']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('year_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Выберете год рождения.</div>';
  }
  if ($errors['sex']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sex_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Выберете пол.</div>';
  }
  //if ($errors['sp']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    //setcookie('sp_error', '', 100000);
    // Выводим сообщение.
    //$messages[] = '<div class="error">Выберете суперспособность(-ти).</div>';
  //}
  if ($errors['limbs']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('limbs_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Укажите количество конечностей.</div>';
  }
  if ($errors['checkbox']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('checkbox_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Ознакомьтесь с контрактом.</div>';
  }
  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
  $values['sp'] = empty($_COOKIE['sp_value']) ? '' : $_COOKIE['sp_value'];
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];

  // Включаем содержимое файла index.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

else{
// Проверяем ошибки.
$errors = FALSE;
$pattern = '/^([А-Я]{1}[а-яё]{1,23}|[A-Z]{1}[a-z]{1,23})$/u';
if (preg_match($pattern, $_POST['name'])) {
 // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('name_value', $_POST['name'], time() + 12 * 30 * 24 * 60 * 60); 
}
else {
 // Выдаем куку на день с флажком об ошибке в поле name.
  setcookie('name_error_value', $_POST['name'], time() + 24 * 60 * 60);
  setcookie('name_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
//\w+@[a-z|а-я]+\.[a-z|а-я]+
$pattern = '/\w+@[a-z]+\.[a-z]+/';
if (preg_match($pattern, $_POST['email'])) {
 setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
}
else {
  setcookie('email_error_value', $_POST['email'], time() + 24 * 60 * 60);
 setcookie('email_error', '1', time() + 24 * 60 * 60);
 $errors = TRUE;
}

if (empty($_POST['sex'])) {
  setcookie('sex_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('sex_value', $_POST['sex'], time() + 12 * 30 * 24 * 60 * 60);
}

if (empty($_POST['yob'])) {
  setcookie('year_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('year_value', $_POST['year'], time() + 12 * 30 * 24 * 60 * 60);
}

if (empty($_POST['limbs'])) {
  setcookie('limbs_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('limbs_value', $_POST['limbs'], time() + 12 * 30 * 24 * 60 * 60);
}

if (empty($_POST['superpowers'])) {
  setcookie('sp_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('sp_value', $_POST['superpowers'], time() + 12 * 30 * 24 * 60 * 60);
}

if (empty($_POST['limbs'])) {
  // Выдаем куку на день с флажком об ошибке в поле name.
  setcookie('limbs_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('limbs_value', $_POST['limbs'], time() + 12 * 30 * 24 * 60 * 60);
}

if (empty($_POST['checkbox'])) {
  // Выдаем куку на день с флажком об ошибке в поле name.
  setcookie('checkbox_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}

if ($errors) {
  // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
  header('Location: index.php');
  exit();
}
else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('yob_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('sp_error', '', 100000);
    setcookie('limbs_error', '', 100000);
}

// Сохранение в базу данных.

// Подготовленный запрос. Не именованные метки.
try {
  $conn = new PDO("mysql:host=localhost;dbname=u20937", 'u20937', '6902593', array(PDO::ATTR_PERSISTENT => true));

    $user = $conn->prepare("INSERT INTO users SET name = ?, email = ?, year = ?, sex = ?, limbs = ?, biography = ?");
    $user -> execute([$_POST['name'], $_POST['email'], $_POST['year'], $_POST['sex'], $_POST['limbs'], $_POST['biography']]);
    $id_user = $conn->lastInsertId();

    $abilitys = $conn->prepare("INSERT INTO abilitys SET id_user = ?");
    $abilitys -> execute([$id_user]);
    $id_abil = $conn->lastInsertId();

    $abil = implode(',',$_POST['superpowers']);

    $ability = $conn->prepare("INSERT INTO ability SET abilitysId = ?, names_ability = ?");
    $ability -> execute([$id_abil, $abil]);

}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

// Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
